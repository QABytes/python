# Object Oriented Programming in Python

# NOTES
------------
#### 01. Classes

Classes are the building blocks in Object Oriented Programming. 

Classes can be seen as blueprints from which you create your Instances. 

------------
#### 02. Instances, Instance methods, Instance attributes

------------
#### 03. Class attributes 

Attributes or methods specific to a class are called Class attributes 

Example:

```
class MyClass(object):
    value = 10
    
    def __init__(self):
        pass
```

Here `value` is a class attribute. These are used when certain values need to be set outside a function.

------------
#### 04. The __init__ constructor

The __init__() constructor is a magic method which gets called when a class is instantiated. 

Any attributes set under the __init__() constructor will be instantiated at the time of instance creation.

------------
#### 05. Inheritance (Inheriting {attributes,methods,constructors etc..})
Inheritance is the capability of one class to derive or inherit the properties from some another class. 

The benefits of inheritance are:

It represents real-world relationships well.
It provides reusability of a code. We don’t have to write the same code again and again. 
Also, it allows us to add more features to a class without modifying it.
It is transitive in nature, which means that if class B inherits from another class A, then all the subclasses of B would automatically inherit from class A.

-------------
#### 06. Encapsulation

Encapsulation is one of the fundamental concepts in object-oriented programming (OOP). 
It describes the idea of wrapping data and the methods that work on data within one unit. 
This puts restrictions on accessing variables and methods directly and can prevent the accidental modification of data. 
To prevent accidental change, an object’s variable can only be changed by an object’s method. 
Those type of variables are known as private varibale.

A class is an example of encapsulation as it encapsulates all the data that is member functions, variables, etc.

------------
#### 07. Polymorphism

Polymorphism is a concept of Object Oriented Programming, which means multiple forms or more than one form. 
Polymorphism enables using a single interface with input of different datatypes, different class or may be for different number of inputs.
In python as everything is an object hence by default a function can take anything as an argument but the execution of the function might fail as every function has some logic that it follows.
For example,
len("hello")      # returns 5 as result
len([1,2,3,4,45,345,23,42])     # returns 8 as result
In this case the function len is polymorphic as it is taking string as input in the first case and is taking list as input in the second case.
In python, polymorphism is a way of making a function accept objects of different classes if they behave similarly.

Method overriding is a type of polymorphism in which a child class which is extending the parent class can provide different definition to any function defined in the parent class as per its own requirements.

Method overloading or function overloading is a type of polymorphism in which we can define a number of methods with the same name but with a different number of parameters as well as parameters can be of different types. These methods can perform a similar or different function.
Python doesn't support method overloading on the basis of different number of parameters in functions.

------------
#### 08. Instance methods

------------
#### 09. Multiple Inheritance and method/attribute lookup 

* Any class can inherit from other classes.
* Any python class can inherit from multiple classes at the same time.
* The class that inherits another class is called the Base/Child class.
* The class being inherited by the Child class is called the Parent class.
* The child class inherits any methods and attributes defined in the parent classes.
* Python uses a depth-first method resolution order (MRO) to fetch methods.
* When two classes inherits from the same class, from Python2.3 onwards, the MRO omits the first occurrence of the class.
* This new MRO lookup method applies from Python2.3, and is for the new-style classes.
	*NOTE:* New style classes inherits from the 'object' parent class.

------------
#### 10. Method Resolution Order (MRO)

* Python has a method lookup order, called `MRO` (Method Resolution Order)
* The MRO path can be printed to stdout using `print <class-name>.mro()`
* Python, by default, uses a depth-first lookup path for MRO.

* ie.. Imagine you have four classes, A, B, C, D.

  1. You instance is created from `D`.
  2. `D` inherits from `B` and `C`
  3. `B` inherits from `A`.
  4. Both `C` and `A` has a method with the same name.
  5. Since python follows a depth-first MRO, the method is called from `A`

REFERENCE: Check the code examples in:
  * 14-multiple-inheritance-1.py
  * 15-multiple-inheritance-2.py

In some cases, the inheritance can get quite cumbersome when multiple classes inherit from the same classes, in multiple levels. 

**NOTE** : From Python2.3, the MRO has changed slightly in order to speed up the method lookups.

The MRO lookup now skips/omits the initial occurrences of classes which occurs multiple time in the lookup path.

* Example:
  1. Four classes, A, B, C, D.
  2. D inherits from both B and C
  3. B inherits from A
  4. C inherits from A
  5. Both C and A contains a similar named method.
  6. Your instance in created from class D.
  7. You try a lookup for the method which is named both in A and C.
  8. The usual lookup should be D -> B -> A -> C -> A
  	a. Hence since the method exists in A and C, it should return from A.
  	b. But since the class A is occurring twice and due to the new MRO method, the lookup will be 
  	D -> B -> C -> A.
  9. The lookup should return the method from class C, rather than A.

REFERENCE: Check the code example in:
  * 16-multiple-inheritance-3.py

--------------
#### 11. Decorators

--------------
#### 12. Static methods

--------------
#### 13. Class methods


